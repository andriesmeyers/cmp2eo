$(function(){

    // Shrinking Header
    var shrinkHeader = $('.site-title').height();
    $(window).scroll(function() {

        var scroll = getCurrentScroll();
        if ( scroll >= shrinkHeader ) {
            $('.header-container').addClass('shrink');
            $('.site-title-scroll').addClass('shrink');
            $('.top').fadeIn();
        }
        else {
            $('.header-container').removeClass('shrink');
            $('.site-title-scroll').removeClass('shrink');
            $('.top').fadeOut();
        }
    });
    function getCurrentScroll() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }

    // Scroll To Top
    $('.top').click(function(){
        $('html,body').animate({
            scrollTop:$('html').offset().top});
        return false;
    });
});