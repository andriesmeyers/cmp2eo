<?php
/**
 * Template Name: Archive Template
 * Description: A Page Template for an Archive Page
 */
?>
<?php  get_header(); ?>
<div class="main-container">
    <div class="main wrapper clearfix">
        <aside>
            <?php get_sidebar(); ?>
        </aside>
        <div class="loop-container">
            <?php if (!have_posts()) : ?>
                <article class="panel">
                    <h1>Nothing Found</h1>
                    <p>No results were found.</p>
                </article>
            <?php endif; ?>
            <article class="panel">
                <h1><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h1>
                <p><?php the_post(); the_content(); ?></p>

                <!-- Archive functions -->
                <h2>Archives by month:</h2>
                <ul>
                    <?php wp_get_archives(); ?>
                </ul>
                <h2>Archives by subject:</h2>
                <ul>
                    <?php wp_list_categories(); ?>
                </ul>
            </article>
        </div>
        <a href="#" class="top"><i class="fa fa-arrow-up"></i></a>
    </div>
    <!-- #main -->
</div>
<!-- #main-container -->
<?php get_footer(); ?>