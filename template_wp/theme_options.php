<?php
function opus_register_settings(){
    register_setting('opus_theme_options', 'opus_options', 'bw_validate_options' );
}

add_action( 'admin_init', 'opus_register_settings');

$opus_options = array(
    'footer_copyright' => '&copy; ' . date('Y') . bloginfo('name')
);

function opus_theme_options_page(){
    global $opus_options;
    if (!isset( $_REQUEST['updated']))
        $_REQUEST['updated'] = false;
    ?>
    <div class="wrap">
        <!-- Show Theme name-->
        <?php echo '<h2>' . wp_get_theme() . 'Theme Options' . "</h2>";
        ?>

        <!-- Show notification is form has been saved -->
        <?php if( false !== $_REQUEST['updated']) : ?>
            <div class="updated fade">
                <p><strong><?php echo 'Options Saved' ?></strong></p>
            </div>
        <?php endif; ?>

        <form method="post" action="options.php">

            <?php $settings = get_option('opus_options', $opus_options); ?>
            <?php settings_fields('opus_theme_options'); ?>

            <table>
                <tr valign="top">
                    <th scope="row"><label for="footer_copyright"><?php echo 'Footer Copyright Notice';?></label></th>
                    <td>
                        <input type="text" id="footer_copyright" name="opus_options[footer_copyright]"
                               value="<?php esc_attr($settings['footer_copyright']); ?>"/>
                    </td>
                </tr>
            </table>
            <p class="submit"><input type="submit" class="button-primary" value="Save Options" /></p>
        </form>
    </div>
<?php } ?>
