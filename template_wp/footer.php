<?php
/**
 * Created by PhpStorm.
 * User: dries
 * Date: 18/03/2016
 * Time: 11:58
 */
?>
<div class="footer-container">
    <footer class="wrapper">
        <nav class="navigation-bar">
            <ul>
                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
            </ul>
        </nav>
        <?php  dynamic_sidebar( 'footer-left' ); ?>
        <h5>Copyright © 2016 Andries Meyers | For educational purposes only</h5>
    </footer>
</div>

<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="<?php bloginfo('template_url'); ?>/js/vendor/jquery-1.11.2.min.js"><\/script>')
</script>
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>

<!-- Google Maps -->


<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
<?php wp_footer(); ?>
</body>

</html>