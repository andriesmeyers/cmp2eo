
<?php  get_header(); ?>
<div class="main-container">
    <div class="main wrapper clearfix">

        <!-- TODO aside to bottom -->


        <div class="loop-container">
            <?php
            if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <article class="panel">
                        <header>
                            <h1><a href="#"><?php the_title(); ?></a></h1>
                        </header>
                        <section class="post-content">
                            <?php the_content();?>

                            <a class="twitter-timeline" href="https://twitter.com/driesmeyers" data-widget-id="734339544905089024">Tweets by @driesmeyers</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        </section>
                    </article>
                <?php endwhile;?>
            <?php else : ?>
            <?php endif;?>


            <a href="#" class="top"><i class="fa fa-arrow-up"></i></a>
        </div>
        <!-- #main -->
        <div class="navigationbox">
            <div class="pagenavi">
                <!-- TODO style navigation -->
                <?php get_pagination(); ?>
            </div>
        </div>
    </div>

    <!-- Page Navigation-->

    <!-- #main-container -->
<?php get_footer(); ?>