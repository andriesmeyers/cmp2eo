<?php

/**
 * Created by PhpStorm.
 * User: dries
 * Date: 12/05/2016
 * Time: 16:52
 */
class Profile_Widget extends WP_Widget
{

    public function __construct(){
        // Widget Settings
        $widget_ops = array(
            'classname' => 'profile-widget',
            'description' => 'A widget that displays your profile information in the sidebar.'
        );

        parent::__construct(
            'my-profile-widget', // Base ID
            'Profile Widget', // Name
            $widget_ops
            );
    }

    public function widget( $args, $instance){
        extract( $args );

        // Get the user-selected settings
        $title = apply_filters('widget_title', $instance['title']);
        $image = $instance['image'];
        $name = $instance['name'];
        $details = $instance['details'];

        // Before widget code (defined in the register sidebar function)
        echo $before_widget;

        // Title of the widget - with fallback (before and after defined by register sidebar function-
        echo $before_title;

        if($title){
            echo $title;
        }else{
            echo "Profile";
        }
        echo $after_title;

        echo '<div class="textwidget"><a>';
        echo '<div class="profile-header">';
        echo '<img src="'.$image.'">';
        echo '<h3>';
        echo $name;
        echo '</h3>';
        echo '</div>';
        echo '</a>';
        echo '<div class="profile-details"><p class="profession">';
        echo $details;
        echo '</p></div>';
        echo '</div>';

        echo $after_widget;
    }

    public function form($instance)
    {
        // Setup some default widget settings /*
        $defaults = array(
            'title' => 'Profile',
            'image' => 'Url of profile image goes here...',
            'name' => 'Your name goes here...',
            'details' => ''
        );
        $instance = wp_parse_args( (array)$instance, $defaults);
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
            <input id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>"
                   value="<?php echo $instance['title'];?>"
                   style="width:95%"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('image'); ?>">Image:</label>
            <input id="<?php echo $this->get_field_id('image'); ?>"
                   name="<?php echo $this->get_field_name('image'); ?>"
                   value="<?php echo $instance['image'];?>"
                   style="width:95%"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('name'); ?>">Profile Name:</label>
            <input id="<?php echo $this->get_field_id('name'); ?>"
                   name="<?php echo $this->get_field_name('name'); ?>"
                   value="<?php echo $instance['name'];?>"
                   style="width:95%"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('details'); ?>">Profile Details:</label>
            <textarea class="widefat" rows="16" cols="20"
                    id="<?php echo $this->get_field_id('details'); ?>"
                   name="<?php echo $this->get_field_name('details'); ?>"
                   style="width:95%"><?php echo $instance['details'];?>
            </textarea>

        </p>
        <?php
    }

    public function update( $new_instance, $old_instance){
        $instance = $old_instance;

        // Update widget settings
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['image'] = $new_instance['image'];
        $instance['name'] = $new_instance['name'];
        $instance['details'] = $new_instance['details'];

        return $instance;
    }
}