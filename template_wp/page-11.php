<?php
/**
 * Created by PhpStorm.
 * User: dries
 * Date: 18/03/2016
 * Time: 16:46
 */
?>
<?php  get_header(); ?>
    <!-- Contact Page -->
    <div class="main-container">
        <div class="main wrapper clearfix">

            <div class="loop-container">
                <!-- TODO Style table & Maps (center maps) -->

                <?php
                if(have_posts())
                {
                while(have_posts()){
                    the_post();
                }?>
                <article class="panel">
                    <header>
                        <!-- TODO Hover categories, author,etc.. -->
                        <h1><?php the_title(); ?></h1>


                    </header>
                    <section class="post-content">
                        <?php the_content(); ?>

                    </section>
                    <footer>
                    </footer>
                    <?php }
                    else
                    {
                        echo 'No content available';
                    }  ?>


                </article>
            </div>
            <span href="#" class="top"><i class="fa fa-arrow-up"></i></span>
        </div>
        <!-- #main -->
    </div>
    <!-- #main-container -->

<?php get_footer(); ?>