$(function(){
    $('#rss-2').children().slideUp();
    $('#rss-2').click(function(){
        $(this).children().slideToggle();
    });
    // Shrinking Header
    var shrinkHeader = $('.site-title').height();
    $(window).scroll(function() {

        var scroll = getCurrentScroll();
        if ( scroll >= shrinkHeader ) {
            $('.header-container').addClass('shrink');
            $('.site-title-scroll').addClass('shrink');
            $('.top').fadeIn();
        }
        else {
            $('.header-container').removeClass('shrink');
            $('.site-title-scroll').removeClass('shrink');
            $('.top').fadeOut();
        }
    });
    function getCurrentScroll() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }

    // Scroll To Top
    $('.top').click(function(){
        $('html,body').animate({
            scrollTop:$('html').offset().top});
        return false;
    });

    // Share Panel

    $('.social-toggle').on('click', function() {
        $(this).next().toggleClass('open-menu');
    });

    // Sidebar Panel CSS
    $( ".main aside div h3").next().css( "padding", "1.1rem" );

    // Comments popup
    /*$('.comment-button').click(function(){
        $('.comment-section').animate({
            width: '100%',
            height : '320px',
            opacity : '100'
        });
    });*/
    $(".comment-button").click(function(){
        if($('.comment-section').hasClass('shrink')) {
            $('.comment-section').removeClass('shrink');
        } else {
            $('.comment-section').addClass('shrink');
        }
    });

    $('#rss-2').children().slideUp();
    $('#rss-2').click(function(){
        $(this).children().slideToggle();
    });

});