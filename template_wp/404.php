<?php  get_header(); ?>
<div class="main-container">
    <div class="main wrapper clearfix">

        <div class="loop-container">
            <h1>Page Not Found</h1>
            <p>Sorry, but the page you were trying to view does not exist.</p>

        </div>
        <a href="#" class="top"><i class="fa fa-arrow-up"></i></a>
    </div>
    <!-- #main -->
</div>
<?php get_footer(); ?>
<!-- IE needs 512+ bytes: http://blogs.msdn.com/b/ieinternals/archive/2010/08/19/http-error-pages-in-internet-explorer.aspx -->
