
<?php  get_header(); ?>
<div class="main-container">
    <div class="main wrapper clearfix">
        <aside>
            <?php get_sidebar(); ?>
        </aside>

        <div class="loop-container">
            <?php
            $blog_posts = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => '3'
            ));

            if ($blog_posts->have_posts()) : ?>
            <?php while ($blog_posts->have_posts()) : $blog_posts->the_post(); ?>
                <article class="panel">

                    <header>
                        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                        <p class="post-date"><i class="fa fa-calendar"></i> <?php the_date(); ?> </p>
                        <p class="post-category"><i class="fa fa-folder"></i><?php the_category(','); ?></p>


                    </header>
                    <section class="post-content">
                        <?php the_excerpt();?>
                        <p><a href="<?php the_permalink(); ?>" class="button">More </a></p>
                    </section>
                    <footer>
                        <ul class="post-footer">
                            <li class="post-footer-li external-link"><a href="<?php the_permalink(); ?>">Visit <i class="fa fa-external-link"></i></a></li>
                            <li class="post-footer-li">
                                <!-- TODO style share text && lower tooltip -->
                                <div class="share-button">
                                    <a href="" class="social-toggle">
                                        <i class="fa fa-share-alt"></i>Share</a>
                                    <div class="social-networks">
                                        <ul>
                                            <li class="social-facebook">
                                                <a onclick="window.open('http://www.facebook.com/share.php?u=<?php the_permalink(); ?> /&title=<?php the_title(); ?>', '', 'width=300, height=300')"
                                                   title="Share on Facebook." target="">
                                                    <i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li class="social-twitter">
                                                <a onclick="window.open('http://twitter.com/home?status=<?php the_title(); ?>+<?php the_permalink(); ?>', '_blank', 'width=300, height=300')"
                                                   title="Tweet this!">
                                                    <i class="fa fa-twitter"></i></a>
                                            </li>
                                            <li class="social-gplus">
                                                <a onclick="window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>', '', 'width=300, height=300')"
                                                   title="Share on Google+">
                                                    <i class="fa fa-google-plus"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>


                            </li>
                            <li class="post-footer-li"><a href="<?php the_permalink(); ?>"><i class="fa fa-comment"></i>Reageer</a></li>
                        </ul>
                    </footer>
                </article>
                <?php endwhile;?>
                <?php wp_reset_query(); ?>
            <?php else : ?>
            <?php endif;?>

            <?php
            // Get portfolio posts & blog posts
            $portfolio_posts = new WP_Query( array(
                'post_type' => 'portfolio',
                'posts_per_page' => 1 // Most recent portfolio item
            ) );

            if ($portfolio_posts->have_posts()) : ?>
                <?php while ($portfolio_posts->have_posts()) : $portfolio_posts->the_post(); ?>
                    <article class="panel panel-image-header">
                        <header>
                            <div class="image-header" style="background-image: url(<?php the_field('project_header_image'); ?>"></div>
                            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                            <p class="post-category"><i class="fa fa-folder"></i>
                                <!-- TODO get category link -->
                                <a href="#">
                                    <?php
                                    // Get Portfolio-Categories
                                    $terms = get_the_terms($post->ID, 'portfolio_category' );
                                    if ($terms && ! is_wp_error($terms)) :
                                        $term_names_arr = array();
                                        foreach ($terms as $term) {
                                            $term_names_arr[] = $term->name;
                                        }
                                        $terms_name_str = join( " ", $term_names_arr);
                                    endif;
                                    echo $terms_name_str;
                                    ?>
                                </a>
                            </p>
                            <div class="gallery cf ">
                                <div>
                                    <a href="#"> <img src="<?php the_field('project_image_1'); ?>" /></a>
                                </div>
                                <div>
                                    <a href="#"> <img src="<?php the_field('project_image_2'); ?>" /></a>
                                </div>
                                <div>
                                    <a href="#"> <img src="<?php the_field('project_image_3'); ?>" /></a>
                                </div>
                                <div>
                                    <a href="#"> <img src="<?php the_field('project_image_4'); ?>" /></a>
                                </div>
                                <div>
                                    <a href="#"> <img src="<?php the_field('project_image_5'); ?>" /></a>
                                </div>

                            </div>

                        </header>
                        <section class="post-content">
                            <p><?php the_content(); ?></p>
                        </section>
                        <footer>
                            <ul class="post-footer">
                                <li class="post-footer-li external-link"><a href="<?php the_permalink(); ?>">Visit <i class="fa fa-external-link"></i></a></li>
                                <li class="post-footer-li">
                                    <!-- TODO style share text && lower tooltip -->
                                    <div class="share-button">
                                        <a href="" class="social-toggle">
                                            <i class="fa fa-share-alt"></i>Share</a>
                                        <div class="social-networks">
                                            <ul>
                                                <li class="social-facebook">
                                                    <a onclick="window.open('http://www.facebook.com/share.php?u=<?php the_permalink(); ?> /&title=<?php the_title(); ?>', '', 'width=300, height=300')"
                                                       title="Share on Facebook." target="">
                                                        <i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li class="social-twitter">
                                                    <a onclick="window.open('http://twitter.com/home?status=<?php the_title(); ?>+<?php the_permalink(); ?>', '_blank', 'width=300, height=300')"
                                                       title="Tweet this!">
                                                        <i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li class="social-gplus">
                                                    <a onclick="window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>', '', 'width=300, height=300')"
                                                       title="Share on Google+">
                                                        <i class="fa fa-google-plus"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="post-footer-li"><a href="<?php the_permalink(); ?>"><i class="fa fa-comment"></i>Reageer</a></li>
                            </ul>
                        </footer>
                    </article>
                <?php endwhile;?>
                <?php wp_reset_query(); ?>
            <?php else : ?>
            <?php endif;?>

            <?php
            $portfolio_posts_other = new WP_Query( array(
                'post_type' => 'portfolio',
                'posts_per_page' => 8 // Most recent portfolio item
            ) );?>


                    <article class="panel">
                        <header>
                            <!-- TODO Hover categories, author,etc.. -->
                            <h1><a href="/cmp2eo/wordpress/portfolio">Other Projects</a></h1>
                        </header>
                        <section class="post-content">
                            <!-- TODO cover image to standard size -->
                            <div class="gallery cf ">
                            <?php if ($portfolio_posts_other->have_posts()) : ?>
                                <?php while ($portfolio_posts_other->have_posts()) : $portfolio_posts_other->the_post(); ?>
                                <div>
                                    <a href="<?php the_permalink(); ?>"> <img src="<?php the_field('project_image_1'); ?>" /></a>
                                </div>
                                <?php endwhile;?>
                                <?php wp_reset_query(); ?>
                            <?php else : ?>
                            <?php endif;?>
                            </div>
                        </section>
                        <footer>
                        </footer>
                    </article>


        <a href="#" class="top"><i class="fa fa-arrow-up"></i></a>
    </div>
    <!-- #main -->
</div>
<!-- #main-container -->
<?php get_footer(); ?>