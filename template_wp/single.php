
<?php  get_header(); ?>
<div class="main-container">
    <div class="main wrapper clearfix">
        <aside>
            <?php get_sidebar(); ?>
        </aside>
        <div class="loop-container">
            <!-- No Results found -->
            <?php if ( !have_posts()) : ?>
                <article class="panel">
                    <h1>Niets gevonden</h1>
                    <p>Er zijn geen resultaten gevonden.</p>
                </article>
            <?php endif; ?>

            <?php
            if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <article class="panel">

                        <header>
                            <h1><?php the_title(); ?></h1>
                            <p class="post-date"><i class="fa fa-calendar"></i> <?php the_date(); ?> </p>
                            <p class="post-category"><i class="fa fa-folder"></i><?php the_category(','); ?></p>


                        </header>
                        <section class="post-content">
                            <?php the_content();?>
                        </section>
                        <footer>
                            <ul class="post-footer">
                                <li class="post-footer-li">
                                    <!-- TODO style share text && lower tooltip -->
                                    <div class="share-button">
                                        <a href="#/" class="social-toggle">
                                            <i class="fa fa-share-alt"></i>Share</a>
                                        <div class="social-networks">
                                            <ul>
                                                <li class="social-facebook">
                                                    <a onclick="window.open('http://www.facebook.com/share.php?u=<?php the_permalink(); ?> /&title=<?php the_title(); ?>', '', 'width=300, height=300')"
                                                       title="Share on Facebook." target="">
                                                        <i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li class="social-twitter">
                                                    <a onclick="window.open('http://twitter.com/home?status=<?php the_title(); ?>+<?php the_permalink(); ?>', '_blank', 'width=300, height=300')"
                                                       title="Tweet this!">
                                                        <i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li class="social-gplus">
                                                    <a onclick="window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>', '', 'width=300, height=300')"
                                                       title="Share on Google+">
                                                        <i class="fa fa-google-plus"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>


                                </li>
                                <li class="post-footer-li"><a class="comment-button"><i class="fa fa-comment"></i>Reageer</a></li>
                            </ul>
                        </footer>
                        <section class="comment-section shrink"><?php comments_template(); ?></section>
                    </article>
                <?php endwhile;?>
            <?php else : ?>
            <?php endif;?>
        </div>
        <a href="#" class="top"><i class="fa fa-arrow-up"></i></a>
    </div>
    <h1>Dit is de single page</h1>
    <!-- #main -->
</div>
<!-- #main-container -->

<?php get_footer(); ?>