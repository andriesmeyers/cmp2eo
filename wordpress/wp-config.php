<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cmp2eo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'bitnami');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D)PmE8.&U8G(L*i~3$9riMgjO^_=d&WQaR5Ug*!V]-`~oWnLR[&iQdI qvZaLv^I');
define('SECURE_AUTH_KEY',  'FZ|<lt` K&qfv+NUjj:F:5D@mnV%%j&(;glMoP6ucjiO#hKd=`-X%{eT(&,X8YdG');
define('LOGGED_IN_KEY',    'D:xSLi;$[C!$Cx^b.6#sIlwo9vSaL~vv~t I[g~L,Y_Zzk2irqC(Y2PA/7bYm-H)');
define('NONCE_KEY',        '+k^D$fYV/f6lve;aR+)5o(j2]2=nhr]*i?6.|09{oY?mb/}*JD &=1|}d&iCW4,&');
define('AUTH_SALT',        '|M6?1TJuX|8)B%RrkF]mR~oT0-`fwIvqHwGo$5Ch;3*S-f+)0p/6ECfnMITi{*k0');
define('SECURE_AUTH_SALT', '-Fq5Ob5K*)cvLR{ZozO}Ft_!|bvQj(?YId:D,dEXsEnPewc?4o8bcu-z|IQ3~8X>');
define('LOGGED_IN_SALT',   '|d@8U!mr)A?)H8Ed :+|58|H4-V}F+v.b4QBMD7a-/}^X0of&v|:TGUautbTQWft');
define('NONCE_SALT',       'EHtmeWF8/k?8.]ZY#(qYF$<.-i~u8o`s3Ppyid7]X:sIDEhBKgFv=cY32>F3]`a-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
