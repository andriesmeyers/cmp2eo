<?php
/**
 * Created by PhpStorm.
 * User: dries
 * Date: 18/03/2016
 * Time: 11:54
 */
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php wp_title(); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Touch Icon & Favicon -->
    <link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>apple-touch-icon.png">
    <link rel="android-icon" href="<?php bloginfo('template_url'); ?>android-icon.png">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/x-icon" />


    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/normalize.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/main.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/vendor/share.css">


    <!-- Javascript -->
    <script src="<?php bloginfo('template_url'); ?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?php wp_head(); ?>
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="header-container">
    <header class="wrapper clearfix">
        <h1 class="site-title"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a><br> <span>[ <?php bloginfo('description'); ?> ]</span></h1>
        <h3 class="site-title-scroll"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h3>

        <nav class="navigation-bar">
            <ul>
                <?php wp_nav_menu( array( 'theme_location' => 'primary-menu' ) ); ?>

            </ul>
        </nav>
    </header>
</div>
