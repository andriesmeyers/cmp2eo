<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Shape
 * @since Shape 1.0
 */

get_header(); ?>
<div class="main-container">
<div class="main wrapper clearfix">
    <aside>
        <?php get_sidebar(); ?>
    </aside>
    <div class="loop-container">

    <?php if ( !have_posts()) : ?>
        <article class="panel">
            <h1>Nothing found</h1>
            <p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
        </article>
    <?php else : ?>
        <article class="panel">
            <h1><?php printf( __('Search Results for: %s'), '<span>' . get_search_query() . '</span>'); ?></h1>
            <?php /* Start the Loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <section>
                <h2><a href="<?php the_permalink(); ?>"></a><?php the_title(); ?></h2>
                <span>Posted on: <?php the_date(); ?> - Category: <?php the_category(); ?>
                    <?php the_tags(); ?>
                </span>
                <p><?php the_excerpt(); ?></p>
                </section>
                <hr>
            <?php endwhile; ?>
            <?php get_template_part( 'no-results', 'search' ); ?>
        </article>
    <?php endif; ?>
    </div>
    <a href="#" class="top"><i class="fa fa-arrow-up"></i></a>
</div>
    <h1>Dit is de search page</h1>
    <!-- #main -->
</div>
    <!-- #main-container -->

<?php get_footer(); ?>