
<?php  get_header(); ?>
<div class="main-container">
    <div class="main wrapper clearfix">
        <aside>
            <?php get_sidebar(); ?>
        </aside>
        <div class="loop-container">
            <?php if (!have_posts()) : ?>
                <article>
                    <h1>Nothing Found</h1>
                    <p>No results were found.</p>
                </article>
            <?php endif; ?>
            <?php
            if(have_posts())
            {
            while(have_posts()){
                the_post();
            }?>
            <article class="panel">
                <header>
                    <!-- TODO Hover categories, author,etc.. -->
                    <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>


                </header>
                <section class="post-content">
                    <?php the_content();
                    ?>
                </section>
                <?php }
                else
                {
                    echo 'No content available';
                }  ?>


            </article>

        </div>
        <a href="#" class="top"><i class="fa fa-arrow-up"></i></a>
    </div>
    <!-- #main -->
</div>
<!-- #main-container -->
<?php get_footer(); ?>