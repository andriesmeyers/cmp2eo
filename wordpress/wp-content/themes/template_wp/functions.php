<?php
/**
 * Created by PhpStorm.
 * User: dries
 * Date: 4/03/2016
 * Time: 10:46
 */

add_theme_support( 'post-thumbnails' );

// Menu's
function register_menu_locations() {
    register_nav_menus(
        array(
            'primary-menu' => __( 'Primary Menu' ),
            'footer-menu' => __( 'Footer Menu' )
        )
    );
}
add_action( 'init', 'register_menu_locations' );


// Sidebar
function register_sidebar_locations() {
    /* Register the 'primary' sidebar. */
    register_sidebar(
        array(
            'id'            => 'primary-sidebar',
            'name'          => __( 'Primary Sidebar' ),
            'description'   => __( 'Sidebar on right hand side of the page.' ),
            'before_widget' => '<div id="%1$s" class="panel">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="sidebar-title">',
            'after_title'   => '</h3>'
        )
    );

    register_sidebar(
        array(
            'id'            => 'footer-left',
            'name'          => __( 'Footer Left' ),
            'description'   => __( 'Widgetbar on the left side of the footer. ' ),
            'before_widget' => '<div id="%1$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h4>',
            'after_title'   => '</h4>'
        )
    );

    register_sidebar(
        array(
            'id'            => 'footer-right',
            'name'          => __( 'Footer Right' ),
            'description'   => __( 'Widgetbar on right side of the footer.' ),
            'before_widget' => '<div id="%1$s"">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>'
        )
    );

}

add_action( 'widgets_init', 'register_sidebar_locations' );

// Paginatie
function get_pagination(){
    global $wp_query;

    $total_pages = $wp_query->max_num_pages;

    if($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'current' => $current_page,
            'prev_text' => '',
            'next_text' => ''
        ));
    }
}

// Current menu item for custom post type
add_filter( 'nav_menu_css_class', 'namespace_menu_classes', 10, 2 );
function namespace_menu_classes( $classes , $item ){
    if ( get_post_type() == 'portfolio' ) {
        // remove unwanted classes if found
        $classes = str_replace( 'current_page_parent', '', $classes );
        // find the url you want and add the class you want
        if ( $item->url == '/PATH_TO_YOUR_ARCHIVE' ) {
            $classes = str_replace( 'menu-item', 'menu-item current_page_parent', $classes );
        }
    }
    return $classes;
}

// Tag Cloud

function custom_tag_cloud_widget($args) {
    $args['largest'] = 12; //largest tag
    $args['smallest'] = 12; //smallest tag
    $args['unit'] = 'pt'; //tag font unit
    $args['number'] = 7;

    return $args;
}
add_filter( 'widget_tag_cloud_args', 'custom_tag_cloud_widget' );

// Custom Widget

function opus_register_widgets(){
    require (get_template_directory() . '/inc/widgets.php' );
    register_widget( 'Profile_Widget' );
}
add_action( 'widgets_init', 'opus_register_widgets' );
?>